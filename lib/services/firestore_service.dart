import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:coronagram_app/models/history.dart';
import 'package:coronagram_app/models/publication.dart';
import 'package:coronagram_app/models/user.dart';

class FirestoreService {
  final Firestore _firestore = Firestore.instance;

  // ir a buscar todas las publicaciones
  Future<List<Publication>> fetchPublications() async {
    List<Publication> publicaciones = [];
    await _firestore
        .collection("publicaciones")
        .orderBy("fecha", descending: true)
        .getDocuments()
        .then((snapshots) {
      snapshots.documents.map((doc) {
        publicaciones.add(Publication.fromSnapshot(doc));
      }).toList();
    });
    return publicaciones;
  }

  // ir a buscar todas las publicaciones las 2 funcionan igual
  Future<List<Publication>> fetchPublications2() async {
    List<Publication> publicaciones = [];
    _firestore
        .collection("publicaciones")
        .orderBy("fecha", descending: true)
        .snapshots()
        .listen((result) {
      result.documents.forEach((result) {
        publicaciones.add(Publication.fromSnapshot(result));
      });
    });
    return publicaciones;
  }

  // ir a buscar todas las historias
  Future<List<History>> fetchHistorys() async {
    List<History> historias = [];
    await _firestore.collection("historias").getDocuments().then((snapshots) {
      snapshots.documents.map((doc) {
        historias.add(History.fromSnapshot(doc));
      }).toList();
    });
    return historias;
  }

  // ir a buscar todos los usuarios
  Future<List<User>> fetchUsers() async {
    List<User> usuarios = [];
    await _firestore
        .collection("usuarios")
        .orderBy("fecha", descending: true)
        .getDocuments()
        .then((snapshots) {
      snapshots.documents.map((doc) {
        usuarios.add(User.fromSnapshot(doc));
      }).toList();
    });
    return usuarios;
  }

  // ir a buscar publicaciones de un usuario
  Future<List<Publication>> fetchPublicationsUser(String userId) async {
    List<Publication> publicaciones = [];
    await _firestore
        .collection("publicaciones")
        .getDocuments()
        .then((snapshots) {
      snapshots.documents.map((doc) {
        if (Publication.fromSnapshot(doc).user == userId) {
          publicaciones.add(Publication.fromSnapshot(doc));
        }
      }).toList();
    });
    return publicaciones;
  }

  // ir a buscar historias de un usuario
  Future<List<History>> fetchHistorysUser(String userId) async {
    List<History> historias = [];
    await _firestore
        .collection("historias")
        .orderBy("fecha", descending: false)
        .getDocuments()
        .then((snapshots) {
      snapshots.documents.map((doc) {
        if (History.fromSnapshot(doc).user == userId) {
          historias.add(History.fromSnapshot(doc));
        }
      }).toList();
    });
    return historias;
  }

  // ir a buscar un usuario
  Future<User> fetchUser(String userId) async {
    User usuario;
    await _firestore.collection("usuarios").getDocuments().then((snapshots) {
      snapshots.documents.map((doc) {
        if (User.fromSnapshot(doc).user == userId) {
          return usuario = User.fromSnapshot(doc);
        }
      }).toList();
    });
    return usuario;
  }

  // ir a buscar un usuario
  Future<User> fetchNewUser(String userId, String correo) async {
    List<User> usuarios = [];
    User usuarioNuevo = User();
    await _firestore.collection("usuarios").getDocuments().then((snapshots) {
      snapshots.documents.map((doc) {
        var usuario = User.fromSnapshot(doc);
        usuarios.add(usuario);
      }).toList();
    });
    for (var i = 0; i < usuarios.length; i++) {
      if (usuarios[i].user == userId) {
        return usuarios[i];
      }
    }
    newUser(userId, correo);
    return usuarioNuevo;
  }

  newUser(String userId, String correo) {
    _firestore.collection("usuarios").add({
      "imagen": null,
      "nombre": "Nuevo Usuario",
      "usuario": userId,
      "correo": correo,
      "descripcion": "Soy un nuevo usuario",
      "fecha": DateTime.now(),
    }).then((value) {});
  }

  Future<User> fetchLoginUser(String userEmail) async {
    User usuario;
    await _firestore.collection("usuarios").getDocuments().then((snapshots) {
      snapshots.documents.map((doc) {
        if (User.fromSnapshot(doc).mail == userEmail) {
          return usuario = User.fromSnapshot(doc);
        }
      }).toList();
    });

    return usuario;
  }
}
