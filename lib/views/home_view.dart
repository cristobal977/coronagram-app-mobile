import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:coronagram_app/components/style_component.dart';
import 'package:coronagram_app/models/publication.dart';
import 'package:coronagram_app/models/user.dart';
import 'package:coronagram_app/services/firestore_service.dart';
import 'package:coronagram_app/services/user_repository.dart';
import 'package:coronagram_app/views/history_view.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';
import 'package:intl/intl.dart';

class HomeView extends StatefulWidget {
  @override
  _HomeViewState createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {
  final picker = ImagePicker();
  String usuarioId;
  final fecha = new DateFormat('dd/MM/yyyy');
  final hora = new DateFormat('h:mm a');

  @override
  void initState() {
    super.initState();
    UserRepository().getUid().then((value) {
      setState(() {
        usuarioId = value;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          title: Text(
            "Coronagram",
            style: TextStyle(fontSize: 24, fontFamily: 'Pacifico'),
          ),
          backgroundColor: Colors.deepPurple[800],
          leading: IconButton(
            icon: Icon(
              Icons.camera_alt,
              color: Colors.white,
            ),
            onPressed: getImage,
          ),
        ),
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                margin: EdgeInsets.only(top: 5),
                height: 95,
                child: SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: FutureBuilder(
                    future: FirestoreService().fetchUsers(),
                    builder: (context, snapshot) {
                      if (snapshot.hasData) {
                        return Row(
                          children: List.generate(
                            snapshot.data.length,
                            (index) {
                              return buildHistory(snapshot.data[index]);
                            },
                          ),
                        );
                      }
                      return Row(
                        children: [
                          SizedBox(
                            width: 20,
                          ),
                          Center(
                            child: CircularProgressIndicator(
                              backgroundColor: Colors.deepPurple[800],
                            ),
                          ),
                        ],
                      );
                    },
                  ),
                ),
              ),
              Divider(),
              FutureBuilder(
                future: FirestoreService().fetchPublications(),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    return Column(
                      children: List.generate(snapshot.data.length,
                          (index) => buildPublication(snapshot.data[index])),
                    );
                  }
                  return Column(
                    children: [
                      SizedBox(height: 150),
                      Center(
                        child: CircularProgressIndicator(
                            backgroundColor: Colors.deepPurple[800]),
                      ),
                      SizedBox(height: 10),
                      Text("Cargando...")
                    ],
                  );
                },
              )
            ],
          ),
        ),
      ),
    );
  }

  Container buildHistory(User usuario) {
    return Container(
      margin: EdgeInsets.only(top: 5),
      height: 90,
      width: 90,
      child: InkWell(
        onTap: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => HistoryView(usuario),
            ),
          );
        },
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            buildImageProfile(2, 70, 70, 65, 65, usuario.image),
            usuario.name.length >= 11
                ? Text(usuario.name.substring(0, 11).split(' ')[0])
                : Text(usuario.name)
          ],
        ),
      ),
    );
  }

  Container buildPublication(Publication publication) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: EdgeInsets.only(bottom: 10, top: 5),
            child: FutureBuilder(
              future: FirestoreService().fetchUser(publication.user),
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                          margin: EdgeInsets.only(left: 10, right: 10),
                          child: buildImageProfile(
                              2, 42, 42, 39, 39, snapshot.data.image)),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            snapshot.data.name,
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          Text(
                            hora.format(
                              new DateTime.fromMicrosecondsSinceEpoch(
                                  publication.date.microsecondsSinceEpoch),
                            ),
                          ),
                        ],
                      )
                    ],
                  );
                }
                return CircularProgressIndicator(
                  backgroundColor: Colors.deepPurple[800],
                );
              },
            ),
          ),
          Image.network(publication.image, fit: BoxFit.fill),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  IconButton(
                    icon: Icon(
                      Icons.favorite_border,
                      color: Colors.red,
                    ),
                    onPressed: null,
                  ),
                  Text(publication.like.toString() + " Me gusta"),
                ],
              ),
              Container(
                margin: EdgeInsets.only(right: 5),
                child: Text(
                  fecha.format(
                    new DateTime.fromMicrosecondsSinceEpoch(
                      publication.date.microsecondsSinceEpoch,
                    ),
                  ),
                ),
              ),
            ],
          ),
          Container(
            margin: EdgeInsets.only(left: 10, right: 10, bottom: 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [Text(publication.description)],
            ),
          ),
          Divider()
        ],
      ),
    );
  }

  Future getImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.camera);
    if (pickedFile == null) {
      return;
    }
    setState(() {
      uploadStatusImage(File(pickedFile.path));
    });
  }

  void uploadStatusImage(File sampleImage) async {
    if (sampleImage != null) {
      var timeKey = DateTime.now();
      final StorageReference postImage =
          FirebaseStorage.instance.ref().child("historias");
      final StorageUploadTask uploadTaskImage =
          postImage.child(timeKey.toString() + ".jpg").putFile(sampleImage);
      var imageUrl =
          await (await uploadTaskImage.onComplete).ref.getDownloadURL();
      final firestoreInstance = Firestore.instance;
      firestoreInstance.collection("historias").add({
        "fecha": DateTime.now(),
        "imagen": imageUrl.toString(),
        "usuario": usuarioId,
      }).then((value) {
        print("Se subio la historia :D");
      });
    }
  }
}
