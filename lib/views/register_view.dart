import 'package:coronagram_app/components/es_text.dart';
import 'package:flutter/material.dart';

class RegisterView extends StatefulWidget {
  @override
  _RegisterViewState createState() => _RegisterViewState();
}

class _RegisterViewState extends State<RegisterView> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
        body: Container(
          color: Colors.white,
          child: Stack(
            children: [
              Image.asset("assets/images/login_fondo.png"),
              Center(child: loginCard())
            ],
          ),
        ),
      ),
    );
  }

  Container loginCard() {
    return Container(
      width: 350,
      padding: EdgeInsets.symmetric(horizontal: 20.0),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(
            height: 200.0,
          ),
          Container(
            padding: EdgeInsets.symmetric(vertical: 10.0),
            child: Text(
              Words().newPassword,
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ),
          TextFormField(
            decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: Words().email,
                focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.deepPurple[800]))),
          ),
           TextFormField(
            decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: Words().password,
                focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.deepPurple[800]))),
          ),
          MaterialButton(
            child: Text(
              Words().newAccount,
              style: TextStyle(color: Colors.white),
            ),
            color: Colors.deepPurple[800],
            onPressed: () {},
          ),
        ],
      ),
    );
  }
}
