import 'package:coronagram_app/components/style_component.dart';
import 'package:coronagram_app/models/history.dart';
import 'package:coronagram_app/models/publication.dart';
import 'package:coronagram_app/models/user.dart';
import 'package:coronagram_app/services/firestore_service.dart';
import 'package:flutter/material.dart';

class SeeProfileView extends StatefulWidget {
  final User usuario;
  SeeProfileView(this.usuario);

  @override
  _SeeProfileViewState createState() => _SeeProfileViewState();
}

class _SeeProfileViewState extends State<SeeProfileView> {
  double profileHeight = 200;
  List<History> historias;
  List<Publication> publicaciones;
  @override
  void initState() {
    super.initState();
    FirestoreService().fetchHistorysUser(widget.usuario.user).then((value) {
      setState(() {
        historias = value;
      });
    });
    FirestoreService().fetchPublicationsUser(widget.usuario.user).then((value) {
      setState(() {
        publicaciones = value;
      });
    });
  }

  List<Widget> _profileChildren;
  List<Widget> _heightWidgets(BuildContext context) {
    _profileChildren ??= List.generate(
      1,
      (index) {
        final height = profileHeight.clamp(
          50.0,
          MediaQuery.of(context).size.width,
        );
        return Container(
          height: height,
          child: Center(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                CircleAvatar(
                  backgroundImage: NetworkImage(widget.usuario.image),
                  radius: 50.0,
                ),
                SizedBox(
                  height: 10.0,
                ),
                Text(
                  widget.usuario.name,
                  style: TextStyle(fontSize: 22.0, color: Colors.black54),
                ),
                SizedBox(
                  height: 2.0,
                ),
                Text(
                  widget.usuario.description,
                  style: TextStyle(fontSize: 15.0, color: Colors.black45),
                ),
                SizedBox(
                  height: 1.0,
                ),
                Text(
                  widget.usuario.mail,
                  style: TextStyle(fontSize: 15.0, color: Colors.black45),
                ),
              ],
            ),
          ),
        );
      },
    );

    return _profileChildren;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(false, "Coronagram"),
      body: widget.usuario != null
          ? DefaultTabController(
              length: 2,
              child: NestedScrollView(
                headerSliverBuilder: (context, _) {
                  return [
                    SliverList(
                      delegate: SliverChildListDelegate(
                        _heightWidgets(context),
                      ),
                    ),
                  ];
                },
                body: Column(
                  children: <Widget>[
                    TabBar(
                      labelColor: Colors.black,
                      indicatorColor: Colors.deepPurple[800],
                      tabs: [
                        Tab(text: 'Fotos'),
                        Tab(text: 'Historias'),
                      ],
                    ),
                    Expanded(
                      child: TabBarView(
                        children: [
                          GridView.count(
                            padding: EdgeInsets.zero,
                            crossAxisCount: 3,
                            children: List.generate(
                              publicaciones != null ? publicaciones.length : 0,
                              (index) => Container(
                                decoration: BoxDecoration(
                                  image: DecorationImage(
                                    fit: BoxFit.fill,
                                    image: NetworkImage(
                                        publicaciones[index].image),
                                  ),
                                ),
                                height: 150.0,
                              ),
                            ),
                          ),
                          ListView(
                            padding: EdgeInsets.zero,
                            children: List.generate(
                              historias != null ? historias.length : 0,
                              (index) => Image.network(
                                historias[index].image,
                                fit: BoxFit.fill,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            )
          : Center(
              child: CircularProgressIndicator(
                backgroundColor: Colors.deepPurple[800],
              ),
            ),
    );
  }
}
