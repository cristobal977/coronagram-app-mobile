import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:coronagram_app/components/style_component.dart';
import 'package:coronagram_app/services/user_repository.dart';
import 'package:coronagram_app/views/navigator_view.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';

class AddPhotoView extends StatefulWidget {
  @override
  _AddPhotoViewState createState() => _AddPhotoViewState();
}

class _AddPhotoViewState extends State<AddPhotoView> {
  String usuarioId;
  File sampleImage;
  String imagen;
  String descripcion = "";
  String url;
  final formKey = GlobalKey<FormState>();
  final picker = ImagePicker();

  @override
  void initState() {
    super.initState();
    UserRepository().getUid().then((value) {
      setState(() {
        usuarioId = value;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(false, "Coronagram"),
      body: SingleChildScrollView(
        child: sampleImage == null
            ? InkWell(
                onTap: getImage,
                child: Container(
                  margin: EdgeInsets.all(20),
                  height: 400,
                  decoration: buildBoxShadow(Colors.white),
                  child: Center(
                    child: Text(
                      "Seleccione una imagen",
                      style: TextStyle(fontSize: 20, color: Colors.black),
                    ),
                  ),
                ),
              )
            : enableUpload(),
      ),
      floatingActionButton: sampleImage == null
          ? null
          : FloatingActionButton(
              backgroundColor: Colors.deepPurple[800],
              onPressed: () {
                if (sampleImage != null && descripcion != "") {
                  uploadStatusImage();
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => NavigatorView()),
                  );
                }
              },
              tooltip: "Agregar imagen",
              child: Icon(
                Icons.send,
              ),
            ),
    );
  }

  Future getImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.gallery);
    if (pickedFile == null) {
      return;
    }
    setState(() {
      sampleImage = File(pickedFile.path);
    });
  }

  Widget enableUpload() {
    return SingleChildScrollView(
      child: Container(
        child: Form(
          key: formKey,
          child: Column(
            children: <Widget>[
              Container(
                margin: EdgeInsets.all(20),
                height: 400,
                decoration: buildBoxShadow(Colors.white),
                child: Image.file(
                  sampleImage,
                  fit: BoxFit.fill,
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 20, right: 20),
                decoration: buildBoxShadow(Colors.white),
                child: Container(
                  padding: EdgeInsets.only(left: 10),
                  child: TextField(
                    keyboardType: TextInputType.multiline,
                    maxLines: 3,
                    style: TextStyle(fontSize: 20),
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      hintText: "Descripcion",
                    ),
                    onChanged: (value) {
                      setState(() {
                        descripcion = value;
                      });
                    },
                  ),
                ),
              ),
              SizedBox(
                height: 15.0,
              ),
            ],
          ),
        ),
      ),
    );
  }

  BoxDecoration buildBoxShadow(Color color) {
    return BoxDecoration(
      color: color,
      borderRadius: BorderRadius.circular(10),
      boxShadow: [
        BoxShadow(
          color: Colors.grey.withOpacity(0.5),
          spreadRadius: 5,
          blurRadius: 7,
          offset: Offset(0, 3),
        ),
      ],
    );
  }

  void uploadStatusImage() async {
    if (descripcion != "" && sampleImage != null) {
      var timeKey = DateTime.now();
      final StorageReference postImage =
          FirebaseStorage.instance.ref().child("publicaciones");

      final StorageUploadTask uploadTaskImage =
          postImage.child(timeKey.toString() + ".jpg").putFile(sampleImage);
      var imageUrl =
          await (await uploadTaskImage.onComplete).ref.getDownloadURL();
      imagen = imageUrl.toString();
      saveToDatabase();
    }
  }

  void saveToDatabase() {
    final firestoreInstance = Firestore.instance;
    firestoreInstance.collection("publicaciones").add({
      "descripcion": descripcion,
      "fecha": DateTime.now(),
      "imagen": imagen,
      "like": 0,
      "usuario": usuarioId,
    }).then((value) {
      print("Se subio la publicacion :D");
    });
  }
}
