import 'package:coronagram_app/components/style_component.dart';
import 'package:coronagram_app/models/user.dart';
import 'package:coronagram_app/services/firestore_service.dart';
import 'package:coronagram_app/views/see_profile_view.dart';
import 'package:flutter/material.dart';

class SearchView extends StatefulWidget {
  @override
  _SearchViewState createState() => _SearchViewState();
}

class _SearchViewState extends State<SearchView> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
        appBar: buildAppBar(false, "Coronagram"),
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              buildSearchBar(),
              Container(
                margin: EdgeInsets.only(left: 10, top: 10),
                child: Text(
                  "Populares",
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
              ),
              FutureBuilder(
                future: FirestoreService().fetchUsers(),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    return Column(
                      children: List.generate(
                        snapshot.data.length <= 4 ? snapshot.data.length : 4,
                        (index) => buildPopularList(snapshot.data[index]),
                      ),
                    );
                  }
                  return Column(
                    children: [
                      SizedBox(height: 150),
                      Center(
                        child: CircularProgressIndicator(
                          backgroundColor: Colors.deepPurple[800],
                        ),
                      ),
                      SizedBox(height: 10),
                      Text("Cargando...")
                    ],
                  );
                },
              )
            ],
          ),
        ),
      ),
    );
  }

  InkWell buildPopularList(User usuario) {
    return InkWell(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => SeeProfileView(usuario),
          ),
        );
      },
      child: ListTile(
        leading: buildImageProfile(2, 42, 42, 39, 39, usuario.image),
        title: Text(usuario.name),
        subtitle: Text(usuario.mail),
      ),
    );
  }

  Container buildSearchBar() {
    return Container(
      padding: EdgeInsets.all(8.0),
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(
            color: Colors.grey[100],
          ),
        ),
      ),
      child: TextField(
        decoration: InputDecoration(
          border: InputBorder.none,
          hintText: "Buscar",
          icon: Icon(
            Icons.search,
            color: Colors.black,
          ),
          hintStyle: TextStyle(
            color: Colors.grey[400],
          ),
        ),
      ),
    );
  }
}
