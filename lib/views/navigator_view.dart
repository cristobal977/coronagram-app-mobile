import 'package:coronagram_app/models/user.dart';
import 'package:coronagram_app/services/firestore_service.dart';
import 'package:coronagram_app/services/user_repository.dart';
import 'package:coronagram_app/views/add_photo_view.dart';
import 'package:coronagram_app/views/home_view.dart';
import 'package:coronagram_app/views/profile_view.dart';
import 'package:coronagram_app/views/search_view.dart';
import 'package:flutter/material.dart';

class NavigatorView extends StatefulWidget {
  @override
  _BottomNavBarState createState() => _BottomNavBarState();
}

class _BottomNavBarState extends State<NavigatorView> {
  int _index = 0;
  String correo;
  String usuarioId;
  User user;
  @override
  void initState() {
    super.initState();
    UserRepository().getEmail().then((value) {
      setState(() {
        correo = value;
      });
    });
    UserRepository().getUid().then((value) {
      setState(() {
        usuarioId = value;
        FirestoreService().fetchNewUser(usuarioId, correo).then((value) {
          setState(() {
            user = value;
          });
        });
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    Widget child;
    switch (_index) {
      case 0:
        child = HomeView();
        break;
      case 1:
        child = SearchView();
        break;
      case 2:
        child = AddPhotoView();
        break;
      case 3:
        child = ProfileView();
        break;
    }

    return Container(
      child: Scaffold(
        body: SizedBox.expand(
          child: child,
        ),
        bottomNavigationBar: Container(
          child: BottomNavigationBar(
            currentIndex: _index,
            onTap: (i) => setState(() => _index = i),
            showSelectedLabels: false,
            items: [
              BottomNavigationBarItem(
                backgroundColor: Colors.deepPurple[800],
                icon: Icon(Icons.home),
                label: "Home",
              ),
              BottomNavigationBarItem(
                backgroundColor: Colors.deepPurple[800],
                icon: Icon(Icons.search),
                label: "Search",
              ),
              BottomNavigationBarItem(
                backgroundColor: Colors.deepPurple[800],
                icon: Icon(Icons.add_box),
                label: "Add",
              ),
              BottomNavigationBarItem(
                backgroundColor: Colors.deepPurple[800],
                icon: Icon(Icons.person),
                label: "Profile",
              )
            ],
          ),
        ),
      ),
    );
  }
}
