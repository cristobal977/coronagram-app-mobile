import 'package:flutter/material.dart';
import 'package:coronagram_app/services/user_repository.dart';
import 'package:coronagram_app/views/register/register_screen.dart';

class CreateAccountButton extends StatelessWidget {
  final UserRepository _userRepository;

  CreateAccountButton({Key key, @required UserRepository userRepository})
      : assert(userRepository != null),
        _userRepository = userRepository,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (context) {
              return RegisterScreen(
                userRepository: _userRepository,
              );
            },
          ),
        );
      },
      child: Center(
        child: Text(
          "¿No tienes Cuenta?",
          style: TextStyle(fontSize: 16),
        ),
      ),
    );
  }
}
