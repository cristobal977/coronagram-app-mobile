import 'package:flutter/material.dart';

class LoginButton extends StatelessWidget {
  final VoidCallback _onPressed;

  LoginButton({Key key, VoidCallback onPressed})
      : _onPressed = onPressed,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        gradient: LinearGradient(
          colors: [
            Color.fromRGBO(69, 39, 160, 1),
            Color.fromRGBO(69, 39, 160, .9),
          ],
        ),
      ),
      child: MaterialButton(
        minWidth: 155,
        onPressed: _onPressed,
        child: Center(
          child: Text(
            "Iniciar Sesion",
            style: TextStyle(color: Colors.white, fontSize: 16),
          ),
        ),
      ),
    );
  }
}
