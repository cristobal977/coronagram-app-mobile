import 'package:coronagram_app/components/style_component.dart';
import 'package:coronagram_app/models/history.dart';
import 'package:coronagram_app/models/user.dart';
import 'package:coronagram_app/services/firestore_service.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class HistoryView extends StatefulWidget {
  final User usuario;
  HistoryView(this.usuario);

  @override
  _HistoryViewState createState() => _HistoryViewState();
}

class _HistoryViewState extends State<HistoryView> {
  List<History> historias;
  final hora = new DateFormat('h:mm a');
  @override
  void initState() {
    super.initState();
    FirestoreService().fetchHistorysUser(widget.usuario.user).then((value) {
      setState(() {
        historias = value;
      });
    });
  }

  int contador = 0;
  @override
  Widget build(BuildContext context) {
    return historias != null
        ? Scaffold(
            appBar: buildAppBar(false, "Coronagram"),
            body: Builder(
              builder: (context) {
                if (historias.length == 0) {
                  return Stack(
                    children: [
                      Container(
                        color: Colors.deepPurple[200],
                        child: InkWell(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: Center(
                              child: Text(
                            widget.usuario.name + " no ha subido una historia",
                            style: TextStyle(fontSize: 20, color: Colors.white),
                          )),
                        ),
                      ),
                      Positioned(
                        left: 10,
                        top: 10,
                        child: usuarioImage(null),
                      )
                    ],
                  );
                } else if (historias.length == 1) {
                  return Stack(
                    children: [
                      InkWell(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: SizedBox.expand(
                          child: Image.network(
                            historias[0].image,
                            fit: BoxFit.fill,
                          ),
                        ),
                      ),
                      Positioned(
                        left: 10,
                        top: 10,
                        child: usuarioImage(historias[0]),
                      )
                    ],
                  );
                } else {
                  return Stack(
                    children: [
                      contador < historias.length - 1
                          ? InkWell(
                              onTap: () {
                                setState(() {
                                  contador++;
                                });
                              },
                              child: SizedBox.expand(
                                child: Image.network(
                                  historias[contador].image,
                                  fit: BoxFit.fill,
                                ),
                              ),
                            )
                          : InkWell(
                              onTap: () {
                                Navigator.pop(context);
                              },
                              child: SizedBox.expand(
                                child: Image.network(
                                  historias[contador].image,
                                  fit: BoxFit.fill,
                                ),
                              ),
                            ),
                      Positioned(
                        left: 10,
                        top: 10,
                        child: usuarioImage(historias[contador]),
                      )
                    ],
                  );
                }
              },
            ),
          )
        : Scaffold(
            body: Center(
              child: CircularProgressIndicator(
                backgroundColor: Colors.deepPurple[800],
              ),
            ),
          );
  }

  Container usuarioImage(History history) {
    return Container(
      child: Row(
        children: [
          buildImageProfile(1, 42, 42, 39, 39, widget.usuario.image),
          SizedBox(width: 8),
          history != null
              ? Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      widget.usuario.name,
                      style: TextStyle(
                          fontWeight: FontWeight.bold, color: Colors.white),
                    ),
                    Text(
                      hora.format(
                        new DateTime.fromMicrosecondsSinceEpoch(
                            history.date.microsecondsSinceEpoch),
                      ),
                      style: TextStyle(
                          fontWeight: FontWeight.bold, color: Colors.white),
                    ),
                  ],
                )
              : Text(
                  widget.usuario.name,
                  style: TextStyle(
                      fontWeight: FontWeight.bold, color: Colors.white),
                ),
        ],
      ),
    );
  }
}
