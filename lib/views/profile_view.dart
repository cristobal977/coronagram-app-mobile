import 'package:coronagram_app/bloc/authentication_bloc/bloc.dart';
import 'package:coronagram_app/components/simple_account_menu.dart';
import 'package:coronagram_app/models/history.dart';
import 'package:coronagram_app/models/publication.dart';
import 'package:coronagram_app/models/user.dart';
import 'package:coronagram_app/services/firestore_service.dart';
import 'package:coronagram_app/services/user_repository.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ProfileView extends StatefulWidget {
  @override
  _ProfileViewState createState() => _ProfileViewState();
}

class _ProfileViewState extends State<ProfileView> {
  double profileHeight = 200;
  String usuarioId;

  User user;
  List<History> historias;
  List<Publication> publicaciones;
  @override
  void initState() {
    super.initState();
    UserRepository().getUid().then((value) {
      setState(() {
        usuarioId = value;
        FirestoreService().fetchUser(usuarioId).then((value) {
          setState(() {
            user = value;
          });
        });
        FirestoreService().fetchHistorysUser(usuarioId).then((value) {
          setState(() {
            historias = value;
          });
        });
        FirestoreService().fetchPublicationsUser(usuarioId).then((value) {
          setState(() {
            publicaciones = value;
          });
        });
      });
    });
  }

  List<Widget> _profileChildren;
  List<Widget> _heightWidgets(BuildContext context) {
    _profileChildren ??= List.generate(
      1,
      (index) {
        final height = profileHeight.clamp(
          50.0,
          MediaQuery.of(context).size.width,
        );
        return Container(
          height: height,
          child: Center(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                CircleAvatar(
                  backgroundImage: NetworkImage(user.image),
                  radius: 50.0,
                ),
                SizedBox(
                  height: 10.0,
                ),
                Text(
                  user.name,
                  style: TextStyle(fontSize: 22.0, color: Colors.black54),
                ),
                SizedBox(
                  height: 2.0,
                ),
                Text(
                  user.description,
                  style: TextStyle(fontSize: 15.0, color: Colors.black45),
                ),
                SizedBox(
                  height: 1.0,
                ),
                Text(
                  user.mail,
                  style: TextStyle(fontSize: 15.0, color: Colors.black45),
                ),
              ],
            ),
          ),
        );
      },
    );

    return _profileChildren;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Text(
          "Coronagram",
          style: TextStyle(fontSize: 24, fontFamily: 'Pacifico'),
        ),
        backgroundColor: Colors.deepPurple[800],
        actions: [
          SimpleAccountMenu(
            icons: [
              Icon(Icons.exit_to_app),
              Icon(Icons.person),
            ],
            iconColor: Colors.white,
            onChange: (index) {
              if (index == 0) {
                BlocProvider.of<AuthenticationBloc>(context).add(LoggedOut());
              }
              if (index == 1) {
                print("Editar perfil en mantencion :D");
              }
            },
          ),
          SizedBox(width: 5)
        ],
      ),
      body: user != null
          ? DefaultTabController(
              length: 2,
              child: NestedScrollView(
                headerSliverBuilder: (context, _) {
                  return [
                    SliverList(
                      delegate: SliverChildListDelegate(
                        _heightWidgets(context),
                      ),
                    ),
                  ];
                },
                body: Column(
                  children: <Widget>[
                    TabBar(
                      labelColor: Colors.black,
                      indicatorColor: Colors.deepPurple[800],
                      tabs: [
                        Tab(text: 'Fotos'),
                        Tab(text: 'Historias'),
                      ],
                    ),
                    Expanded(
                      child: TabBarView(
                        children: [
                          GridView.count(
                            padding: EdgeInsets.zero,
                            crossAxisCount: 3,
                            children: List.generate(
                              publicaciones != null ? publicaciones.length : 0,
                              (index) => Container(
                                decoration: BoxDecoration(
                                  image: DecorationImage(
                                    fit: BoxFit.fill,
                                    image: NetworkImage(
                                        publicaciones[index].image),
                                  ),
                                ),
                                height: 150.0,
                              ),
                            ),
                          ),
                          ListView(
                            padding: EdgeInsets.zero,
                            children: List.generate(
                              historias != null ? historias.length : 0,
                              (index) => Image.network(
                                historias[index].image,
                                fit: BoxFit.fill,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            )
          : Center(
              child: CircularProgressIndicator(
                backgroundColor: Colors.deepPurple[800],
              ),
            ),
    );
  }
}
