import 'package:coronagram_app/components/es_text.dart';
import 'package:coronagram_app/components/style_component.dart';
import 'package:flutter/material.dart';

class RequestPasswordView extends StatefulWidget {
  @override
  _RequestPasswordView createState() => _RequestPasswordView();
}

class _RequestPasswordView extends State<RequestPasswordView> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
        appBar: buildAppBar(true, Words().coronagram),
        body: Container(
        color: Colors.white,
          child: Stack(
            children: [
              Image.asset("assets/images/login_fondo.png"),
              Center(child: loginCard())
            ],
          ),
        ),
      ),
    );
  }

  Container loginCard() {
    return Container(
        width: 350,
        padding: EdgeInsets.symmetric(horizontal: 20.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(
            height: 200.0,
          ),
            Container(
              padding: EdgeInsets.symmetric(vertical: 10.0),
              child: Text(
                Words().enterPassword,
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            TextFormField(
                  decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: Words().email,
                      focusedBorder: OutlineInputBorder(
                          borderSide:
                              BorderSide(color: Colors.deepPurple[800]))),
            ),
            MaterialButton(
              child: Text(
                Words().requestPassword,
                style: TextStyle(color: Colors.white),
              ),
              color: Colors.deepPurple[800],
              onPressed: () {},
            ),
          ],
        ),
    );
  }
}
