import 'package:cloud_firestore/cloud_firestore.dart';

class Publication {
  final String id, image, description, user;
  final Timestamp date;

  final int like;

  Publication({
    this.id,
    this.image,
    this.description,
    this.date,
    this.user,
    this.like,
  });

  factory Publication.fromSnapshot(DocumentSnapshot snapshot) {
    return Publication(
      id: snapshot.documentID,
      image: snapshot.data["imagen"],
      description: snapshot.data["descripcion"],
      date: snapshot.data["fecha"],
      user: snapshot.data["usuario"],
      like: snapshot.data["like"],
    );
  }
}
