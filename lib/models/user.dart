import 'package:cloud_firestore/cloud_firestore.dart';

class User {
  final String id, name, description, mail, image, user;
  final Timestamp date;

  User({
    this.id,
    this.name,
    this.description,
    this.mail,
    this.image,
    this.user,
    this.date,
  });

  factory User.fromSnapshot(DocumentSnapshot snapshot) {
    return User(
        id: snapshot.documentID,
        name: snapshot.data["nombre"],
        description: snapshot.data["descripcion"],
        mail: snapshot.data["correo"],
        image: snapshot.data["imagen"] != "" && snapshot.data["imagen"] != null
            ? snapshot.data["imagen"]
            : "https://firebasestorage.googleapis.com/v0/b/coronagram-1cb87.appspot.com/o/usuarios%2FnuevoUser.png?alt=media&token=c1113733-32be-4954-a151-9227e18d9512",
        user: snapshot.data["usuario"],
        date: snapshot.data["fecha"]);
  }
}
