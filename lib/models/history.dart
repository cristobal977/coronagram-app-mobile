import 'package:cloud_firestore/cloud_firestore.dart';

class History {
  final String id, image, user;
  final Timestamp date;

  History({
    this.id,
    this.image,
    this.date,
    this.user,
  });
  factory History.fromSnapshot(DocumentSnapshot snapshot) {
    return History(
      id: snapshot.documentID,
      image: snapshot.data["imagen"],
      date: snapshot.data["fecha"],
      user: snapshot.data["usuario"],
    );
  }
}
