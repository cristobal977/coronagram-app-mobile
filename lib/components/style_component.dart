import 'package:flutter/material.dart';

AppBar buildAppBar(
  bool goBack,
  String title,
) =>
    AppBar(
      automaticallyImplyLeading: goBack,
      title: Text(
        title,
        style: TextStyle(fontSize: 24, fontFamily: 'Pacifico'),
      ),
      backgroundColor: Colors.deepPurple[800],
    );

Container buildImageProfile(double sizeborder, double heightBorder,
    double widthBorder, double heightImage, double widthImage, String image) {
  return Container(
    height: heightBorder,
    width: widthBorder,
    decoration: BoxDecoration(
      shape: BoxShape.circle,
      gradient: LinearGradient(
        begin: Alignment.topCenter,
        end: Alignment.bottomCenter,
        colors: [
          Colors.deepPurple[800],
          Colors.deepPurple[200],
        ],
      ),
    ),
    child: Center(
      child: Container(
        height: heightImage,
        width: widthImage,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          border: Border.all(color: Colors.white, width: sizeborder),
          image: DecorationImage(
            fit: BoxFit.fill,
            image: NetworkImage(image),
          ),
        ),
      ),
    ),
  );
}
